#ifndef _ROBO_H_
#define _ROBO_H_

#include <iostream>
#include <libplayerc/playerc.h>

using namespace std;

// as velocidades do robo para a frente, para a esquerda e para a direita respectivamente
#define rob_vel_f 0.1
#define rob_vel_e 0.2
#define rob_vel_d -0.2


class Robo{
	private:
		playerc_client_t *client;			//Cliente para conexão com o servidor
		playerc_position2d_t *position2d;	//Objeto para conexão com o odômetro
	
	public:
		Robo()
		{
			//Conecta ao servidor no endereço “localhost”, na porta 6665 (default)  
			client = playerc_client_create(NULL, "localhost", 6665);
			if (playerc_client_connect(client) != 0) {
				fprintf(stderr, "error: %s\n", playerc_error_str());
			//		return -1;
			}

			//Conecta ao odômetro - motor do robô
			position2d = playerc_position2d_create(client, 0);
			if (playerc_position2d_subscribe(position2d, PLAYERC_OPEN_MODE) != 0) {
				fprintf(stderr, "error: %s\n", playerc_error_str());
			//		return -1;
			}

			//Ativa os motores do robô
			playerc_position2d_enable(position2d, 1);
			playerc_position2d_set_cmd_vel(position2d, 0.0, 0, 0.0, 1);
		}

		void frente()
		{
			playerc_position2d_set_cmd_vel(position2d, rob_vel_f, 0.0, 0.0, 1);
		}

		void esquerda()
		{
			playerc_position2d_set_cmd_vel(position2d, 0.0, 0.0, rob_vel_e, 1);
		}
		
		void direita()
		{
			playerc_position2d_set_cmd_vel(position2d, 0.0, 0.0, rob_vel_d, 1);
		}

		~Robo()
		{
			playerc_position2d_set_cmd_vel(position2d, 0.0, 0, 0.0, 1);
			playerc_position2d_unsubscribe(position2d);
			playerc_position2d_destroy(position2d);
			playerc_client_disconnect(client);
			playerc_client_destroy(client);	
		}

};

#endif
