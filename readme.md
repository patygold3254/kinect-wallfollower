--> valores dos pontos:

	/* ***função para os dados no centro da tela*** */
	
	//retangulo no centro da tela
	ponto_inicial.x = 263;
	ponto_inicial.y = 40;
	ponto_final.x = 374;
	ponto_final.y = 470;
	rectangle(depthMap, ponto_inicial, ponto_final, Scalar(255, 0, 0), 1, CV_AA, 0);

	/* ***função para os dados do lado esquerdo da tela*** */
	
	//retangulo lado esquerdo da tela
	ponto_inicial.x = 10;
	ponto_inicial.y = 40;
	ponto_final.x = 126;
	ponto_final.y = 470;
	rectangle(depthMap, ponto_inicial, ponto_final, Scalar(255, 0, 0), 1, CV_AA, 0);

	/* ***função para os dados do lado direita da tela*** */
	
	//retangulo lado direito da tela
	ponto_inicial.x = 480;
	ponto_inicial.y = 40;
	ponto_final.x = 590;
	ponto_final.y = 470;
	rectangle(depthMap, ponto_inicial, ponto_final, Scalar(255, 0, 0), 1, CV_AA, 0);
