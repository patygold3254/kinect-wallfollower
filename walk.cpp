#include <cv.h>					// biblioteca padrão opencv
#include <highgui.h>			// biblioteca 'alto nível' opencv
#include <string>
#include <iostream>				// entrada padrão c++
#include <limits.h>				// para INT_MAX, INT_MIN
#include "Robo.h"				// classe que cuida da interface com o robo

using namespace cv;
using namespace std;

#define MIN_FRENTE 25
#define MAX_FRENTE 51

#define MIN_ESQUERDA 77
#define MAX_ESQUERDA 257

#define MIN_DIREITA 51
#define MAX_DIREITA 179


/* recebe como parametro os pontos finais e iniciais do retangulo que se deseja fazer o reconhecimento da borda
 * recebe também os pontos das retas no mapa, retas achadas na transformada de Hough 
 * recebe também os pontos do retangulo como pt_inicial e pt_final 												*/
void borda (Point ponto_inicial, Point ponto_final, Mat houghMap, Mat depth, vector<Point> ponto1, vector<Point> ponto2, Point pt_inicial, Point pt_final)
{
	Point aux_pt1;			// auxiliar para o ponto 1 da reta
	Point aux_pt2;			// auxiliar para o ponto 2 da reta
	
	double m;				// para armazenar o coeficiente angular
	
	int tam_ret;			// quantidade de x do retangulo na tela
	int x;					// auxiliar para os pontos do retangulo
	
	tam_ret = pt_final.x - pt_inicial.x;
	
	while(!ponto1.empty())
	{
		m = 0.0;
		
		// pegando os pontos das retas passadas como parametro
		aux_pt1 = ponto1.back();
		ponto1.pop_back();
		aux_pt2 = ponto2.back();
		ponto2.pop_back();
		
		// calculando o coeficiente angular m
		// com o coeficiente angular eu tenho a equação da reta
		// faz esse if antes para não correr o risco de fazer uma divisão por zero
		if((aux_pt1.x - aux_pt2.x) != 0)
		{
			m = (aux_pt1.y - aux_pt2.y)/(aux_pt1.x - aux_pt2.x);
		}
		
		// elimina os pontos com coeficiente 0, ou seja, que estão em paralelo com o chão
		if (m != 0)
		{
			// para usar somente os coeficientes (m) positivo
			/* não sei se ainda é util esta parte
			 * if(m < 0)
			{
				m = m * (-1);
			}
			*/

			// equacao da reta: aux_pt1.y - y = m (aux_pt1.x - x)
			// vendo se a reta passa dentro do retangulo definido anteriormente
			// até que verifique todos os pontos x de dentro do retangulo
			// usa somente o ponto final de y
			for (x = pt_inicial.x; x < pt_final.x; x = x + 2)
			{
				if((aux_pt1.x - x) != 0)
				{
					if (((aux_pt1.y - pt_final.y)/(aux_pt1.x - x)) == m)
					{
						printf("esta dentro\n");
					}
				}
			}
			
			//aqui que eu posso usar o coeficiente angular (m)
			//printf("m: %lf\n",m);
		}
	}
}


/* recebe como parametro os pontos finais e iniciais do retangulo que se deseja pegar os resultados de valor maximo de pixels e minimos
 * retorna a soma dos pixels validos																	*/
int pixels_valid(Point ponto_inicial, Point ponto_final, Mat depthMap, Mat validDepthMap, int *minimo, int *maximo)
{
	int idx_altura, idx_largura;	// indices	
	int valid_pix_sum;				//soma do pixels validos
	
	Scalar intensity;				// intensidade de um pixel
	int dist;						// distancia em cm deste pixel
	
	Scalar intensity_valid;			// pixel valido
	int valid_pix;					// valor do pixel valido		

	*minimo = INT_MAX;				//minimo recebe o maximo do int
	*maximo = INT_MIN;				//maximo recebe o minimo do int
	
	valid_pix_sum = 0;
	valid_pix = 0;
	dist = 0;
	
	
	for(idx_altura = ponto_inicial.y; idx_altura <= ponto_final.y; idx_altura++)
	{
		for(idx_largura = ponto_inicial.x; idx_largura <= ponto_final.x; idx_largura++)
		{
			intensity = depthMap.at<short int>(idx_altura, idx_largura); //intensidade do pixel no mapa de profundidade nos pontos idx_altura e idx_largura
			intensity_valid = validDepthMap.at<uchar>(idx_altura, idx_largura);	//intensidade do pixel no mapa de pixels valido nos pontos idx_altura e idx_largura
			
			dist = intensity.val[0] / 10; //transforma a distancia do pixel no mapa de profundidade para cm (estava em mm)
			valid_pix = intensity_valid.val[0]; //acessa o pixel no mapa de pixels validos
			
			// para computar apenas pixels validos
			if((valid_pix > 0) && (dist > 3))
			{
				if (dist < *minimo)
				{
					*minimo = dist;
				}
				if (dist > *maximo)
				{
					*maximo = dist;
				}
			}

			//soma dos pixels validos
			valid_pix_sum += valid_pix;
		}
	}
	
	return valid_pix_sum;
}

//entra como parametro os valores minimos e maximos, posteriormente adquiridos pela função pixels_valid()
//processamento das imagens adquiridas pelo kinect
int processamento (int *minimo, int *maximo)
{
/******* ROBOT ********/
	//inicia o robo - chama construtor
	Robo *robo = new Robo();
/******* ROBOT ********/


/****** KINECT ******/

	// abertura do kinect
    cout << "Kinect opening ..." << endl;
    VideoCapture capture( CV_CAP_OPENNI );
    cout << "done." << endl;

    if (!capture.isOpened()) {
        cout << "Can not open a capture object." << endl;
        return -1;
    }

/****** KINECT ******/

// matrizes imagem
	Mat depth;			// imagem original de profundidade para processamento
	Mat invalid;		// imagem de bits válidos de profundidade (preto e branco)
	Mat bgrImage;		// imagem colorida
	Mat show;			// imagem de profundidade em tons de cinza para visualização
	Mat depthCanny;		// imagem para usar o Canny
	Mat cdst;			// imagem para a transformada de Hough
	
	Point ponto_inicial;			// ponto final do retangulo
	Point ponto_final;				// ponto inicial do retangulo
	string file;
	
	int valid_pix_sum1;				// soma dos pixels validos para o retangulo 1
	int valid_pix_sum2;				// soma dos pixels validos para o retangulo 2
	int valid_pix_sum3;				// soma dos pixels validos para o retangulo 3	 
	
	bool verificador = false;		// verifica ainda está na frente e o robo está virando a direita (true)
									// false significa parede a esquerda

	int direcao;					// direcao a ser tomada pelo robo	

	
	// criação das janelas
	cvNamedWindow("imagem colorida");
	cvMoveWindow("imagem colorida", 640, 30);

	cvNamedWindow("depth map");
	cvMoveWindow("depth map", 0, 30);
	
// loop principal
	for(;;)
	{

		if( !capture.grab() ) {
			cout << "Can not grab images." << endl;
			break;
		}

		// captura mapa de profundidade e mapa de pixels válidos
		if( !capture.retrieve( depth, CV_CAP_OPENNI_DEPTH_MAP ) ||
			!capture.retrieve (invalid, CV_CAP_OPENNI_VALID_DEPTH_MASK) ||
			!capture.retrieve (bgrImage, CV_CAP_OPENNI_BGR_IMAGE))
		{
			cout << "Nao foi possivel capturar alguma das imagens." << endl;
			break;
		}

		//para imitar a captura da imagem do kinect
/*		depth = imread("file_gray_1327067351_3134.png",0);				// imagem de profundidade
		invalid = imread("file_invalid_1336508682_1311.png",0);			// imagem de pontos invalidos
		bgrImage = imread("file_rgb_1336508682_1311.png",0);			// imagem em rgb
*/
		if( (!depth.data) || (!invalid.data) || (!bgrImage.data) ) // check if the image has been loaded properly
		{
			return -1;
		}

		// converte imagem original para tons de cinza
		depth.convertTo (show, CV_8UC3, 0.05f);
		// tons de cinza em três canais para aceitar elementos coloridos
		cvtColor(show, show, CV_GRAY2RGB);
	
		// decisão de qual lado 'virar'
		direcao = 0;
		
		/* ********************* usando algoritmo Canny e Hough********************** */
		Canny(depth, depthCanny, 5, 20);
		cvtColor(depthCanny, cdst, CV_GRAY2BGR);

		vector<Vec2f> lines;			// vetor para a transformada de Hough
		vector<Point> pontos1;			// vetor para guardar um dos pontos da reta
		vector<Point> pontos2;			// vetor para guardar o segundo ponto da reta
		
		// usando a transformada de Hough padrão
		// a transformada de Hough probabilística não deu resultados satisfatórios 
		HoughLines(depthCanny, lines, 1, CV_PI/180, 100, 0, 0 );
		
		// desenhando a linha de Hough
		// as linhas de Hough ficam na imagem cdst
		for( size_t i = 0; i < lines.size(); i++ )
		{
		  float rho = lines[i][0], theta = lines[i][1];
		  Point pt1, pt2;
		  double a = cos(theta), b = sin(theta);
		  double x0 = a*rho, y0 = b*rho;
		  pt1.x = cvRound(x0 + 1000*(-b));
		  pt1.y = cvRound(y0 + 1000*(a));
		  pt2.x = cvRound(x0 - 1000*(-b));
		  pt2.y = cvRound(y0 - 1000*(a));
		  line(cdst, pt1, pt2, Scalar(255,0,0), 3, CV_AA);
		  pontos1.push_back(pt1);
		  pontos2.push_back(pt2);
		}
		/* ******************************************************************* */
		
		
		/* ***função para os dados no centro da tela*** */
		
		//retangulo no centro da tela
		ponto_inicial.x = 263;
		ponto_inicial.y = 40;
		ponto_final.x = 374;
		ponto_final.y = 470;
		rectangle(show, ponto_inicial, ponto_final, Scalar(0, 255, 0), 1, CV_AA, 0);
		rectangle(bgrImage, ponto_inicial, ponto_final, Scalar(0, 255, 0), 1, CV_AA, 0);
		rectangle(cdst, ponto_inicial, ponto_final, Scalar(0, 255, 0), 1, CV_AA, 0);

		// para o processamento
		valid_pix_sum1 = pixels_valid(ponto_inicial, ponto_final, depth, invalid, minimo, maximo);
		
		// somente faz o processamento se tiver pixels válidos
		// se só tiver pixels inválidos, virar para a direita		
		if (valid_pix_sum1 <= 0)
		{
			printf("\nSomente pixels inválidos\n");
			direcao = 1;
		}
		//se parede em frente, virar a direita até parede a esquerda
		else if ((*maximo <= MAX_FRENTE) && (*minimo >= MIN_FRENTE))
		{
			printf("\nParede em frente -> vire a direita\n");
			verificador = true;
			direcao = 1;
		}
		//se o valor maximo na frente for maior que o MAX_FRENTE
		else if(*maximo > MAX_FRENTE)
		{
			printf("\nParede longe da frente -> siga em frente\n");
			direcao = 2;
		}
		//se o valor minimo na frente for menos que o MIN_FRENTE
		if(*minimo < MIN_FRENTE)
		{
			printf("\nParede perto da frente -> vire a direita\n");
			direcao = 1;
		}

		/* ***função para os dados do lado esquerdo da tela*** */
		
		//retangulo lado esquerdo da tela
		ponto_inicial.x = 10;
		ponto_inicial.y = 40;
		ponto_final.x = 126;
		ponto_final.y = 470;
		rectangle(show, ponto_inicial, ponto_final, Scalar(0, 255, 0), 1, CV_AA, 0);
		rectangle(bgrImage, ponto_inicial, ponto_final, Scalar(0, 255, 0), 1, CV_AA, 0);
		rectangle(cdst, ponto_inicial, ponto_final, Scalar(0, 255, 0), 1, CV_AA, 0);

		// para o processamento
		valid_pix_sum2 = pixels_valid(ponto_inicial, ponto_final, depth, invalid, minimo, maximo);
		// reconhecimento da borda
		borda (ponto_inicial, ponto_final, cdst, depth, pontos1, pontos2, ponto_inicial, ponto_final);

		// somente faz o processamento se tiver pixels válidos
		// se só tiver pixels inválidos, virar para a direita
		if (valid_pix_sum2 <= 0)
		{
			printf("\nSomente pixels inválidos\n");
			direcao = 1;			
		}
		//parede a esquerda, seguir a em frente
		else if ((*maximo <= MAX_ESQUERDA) && (*minimo >= MIN_ESQUERDA))
		{
			printf("\nParede a esquerda -> siga em frente\n");
			verificador = false;
			direcao = 2;
		}
		//parede muito perto da esquerda
		else if (*minimo < MIN_ESQUERDA)
		{
			printf("\nParede muito perto da esquerda esquerda -> virar a direita\n");
			direcao = 1;
		}

		/* ***função para os dados do lado direita da tela*** */
		
		//retangulo lado direito da tela
		ponto_inicial.x = 480;
		ponto_inicial.y = 40;
		ponto_final.x = 590;
		ponto_final.y = 470;
		rectangle(show, ponto_inicial, ponto_final, Scalar(0, 255, 0), 1, CV_AA, 0);
		rectangle(bgrImage, ponto_inicial, ponto_final, Scalar(0, 255, 0), 1, CV_AA, 0);
		rectangle(cdst, ponto_inicial, ponto_final, Scalar(0, 255, 0), 1, CV_AA, 0);

		// mostra a imagem
//		imshow("detected lines", cdst);
//		waitKey();	
				
		valid_pix_sum3 = pixels_valid(ponto_inicial, ponto_final, depth, invalid, minimo, maximo);
		// reconhecimento da borda
		borda (ponto_inicial, ponto_final, cdst, depth, pontos1, pontos2, ponto_inicial, ponto_final);

		// somente faz o processamento se tiver pixels válidos
		// se só tiver pixels inválidos, virar para a direita		
		if (valid_pix_sum1 <= 0)
		{
			printf("\nSomente pixels inválidos\n");
			direcao = 1;			
		}
		//se parede a direita, virar a direita até parede a esquerda
		else if ((*maximo <= MAX_DIREITA) && (*minimo >= MIN_DIREITA))
		{
			printf("\nParede a direita -> vire a direita\n");
			verificador = true;
			direcao = 1;
		}
		//parede muito perto da direita
		else if (*minimo < MIN_DIREITA)
		{
			// se não tiver entrado em outro caso
			if (direcao == 0)
			{
				printf("\nParede muito perto da direita -> vire a esquerda\n");
				direcao = 3;
			}
			// se entrou em outro caso, perigo de indecisão
			// vai em frente
			else
			{
				direcao = 2;
			}
		}
		
		/* ***** virar para a direita até parede a esquerda ****** */
if (verificador == true)
{
	direcao = 1;
}	

/****
notacao binaria para escolher a direcao:
000 -> 
001 -> direita
010 -> frente
011 -> esquerda
100 -> 
101 -> 
110 -> 
111 -> 
****/
		switch (direcao)
		{
			//vai para frente
			case 2:
			{
				robo->frente();
				cout << endl << "COMANDO: FRENTE";
				break;
			}
			case 1:
			{
				robo->direita();
				cout << endl << "COMANDO: DIREITA";
				break;
			}
			case 3:
			{
				robo->esquerda();
				cout << endl << "COMANDO: ESQUERDA";
				break;
			}			
			//default -> ir para a frente
			default:
			{
				robo->frente();
				cout << endl << "COMANDO: FRENTE";
				break;
			}

		}


// mostra as imagens
		imshow("imagem colorida", bgrImage);
		imshow( "depth map", show );

//		wait 0.033 sec - facilitar visualização de dados
		usleep(33333);

//se pressionar alguma tecla, cancela
		if (waitKey (2) >= 0 ) break;

	}

	/****** ROBOT - DESTRUTOR *****/
	delete robo;
	/****** ROBOT - DESTRUTOR *****/

	return 1;
	
}

int main(int argc, char** argv)
{	
	int minimo,maximo;					//distancia minima e maxima
	
	int verificador = processamento(&minimo, &maximo);	//verifica se deu certo a abertura das imagens e 
														//realiza o processamento das mesmas
    								
	if(verificador == -1)
	{
		printf("Erro!!\n");
		return -1;
	}
	
	
	//mostra a imagem
/*	cvNamedWindow("depth map");
	cvMoveWindow("depth map", 0, 30);
	
	imshow("depth map", minimo);
	
	//para a visualizacao da imagem
	waitKey();
*/
	
	return 0;
}
